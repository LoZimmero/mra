package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

public class MarsRoverTest {

	@Test
	@Ignore
	public void test() {
		fail("Not yet implemented");
	}

	@Test
	// User story 1
	public void testMarsRoverConstructorOK() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(4,7)");
		obstacles.add("(2,3)");
		MarsRover mr = new MarsRover(10, 10, obstacles);
		assertEquals(mr.planetContainsObstacleAt(4, 7), true);
		assertEquals(mr.planetContainsObstacleAt(2, 3), true);
		assertEquals(mr.planetContainsObstacleAt(5, 7), false);
	}

	@Test
	// User story 1
	public void testMarsRoverConstructorErrorObstaclesOutOfBound() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(10,7)");
		obstacles.add("(2,3)");
		Exception e = assertThrows(MarsRoverException.class, () -> {
			MarsRover mr = new MarsRover(10, 10, obstacles);
		});
		assertEquals(e.getClass(), MarsRoverException.class);
	}
	
	@Test
	// User story 1
	public void testMarsRoverConstructorErrorObstacleInOrigin() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(0,0)");
		obstacles.add("(2,3)");
		Exception e = assertThrows(MarsRoverException.class, () -> {
			MarsRover mr = new MarsRover(10, 10, obstacles);
		});
		assertEquals(e.getClass(), MarsRoverException.class);
	}
	
	@Test
	// User story 1
	public void testMarsRoverConstructorErrorObstacleStringMalformed() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(1,0)");
		obstacles.add("(2,3");
		Exception e = assertThrows(MarsRoverException.class, () -> {
			MarsRover mr = new MarsRover(10, 10, obstacles);
		});
		assertEquals(e.getClass(), MarsRoverException.class);
	}

	@Test
	// User story 1
	public void testMarsRoverConstructorErrorXorYOutOfBound() throws MarsRoverException {
		// No negative numbers ammitted
		Exception e = assertThrows(MarsRoverException.class, () -> {
			MarsRover mr = new MarsRover(-1, 10, new ArrayList<String>());
		});
		assertEquals(e.getClass(), MarsRoverException.class);

		Exception ex2 = assertThrows(MarsRoverException.class, () -> {
			MarsRover mr = new MarsRover(1, -1, new ArrayList<String>());
		});
		assertEquals(ex2.getClass(), MarsRoverException.class);

		// No zero numbers ammitted
		Exception ex3 = assertThrows(MarsRoverException.class, () -> {
			MarsRover mr = new MarsRover(0, 1, new ArrayList<String>());
		});
		assertEquals(ex3.getClass(), MarsRoverException.class);

		Exception ex4 = assertThrows(MarsRoverException.class, () -> {
			MarsRover mr = new MarsRover(0, 1, new ArrayList<String>());
		});
		assertEquals(ex4.getClass(), MarsRoverException.class);
	}
	
	@Test
	// User story 2
	public void testExecuteCommandEmpty() throws MarsRoverException {
		MarsRover mr = new MarsRover(10, 10, new ArrayList<String>());
		String commandString = "";
		String emptyCommandString = "(0,0,N)";
		String res = mr.executeCommand(commandString);
		assertEquals(res, emptyCommandString);
	}
	
	@Test
	// User story 3
	public void testTurning() throws MarsRoverException {
		MarsRover mr = new MarsRover(10, 10, new ArrayList<String>());
		String commandString = "r";
		String res = mr.executeCommand(commandString);
		assertEquals(res, "(0,0,E)");
		commandString = "l";
		res = mr.executeCommand(commandString);
		assertEquals(res, "(0,0,N)");
	}
	
	@Test
	// User story 4
	public void testGoForward() throws MarsRoverException {
		MarsRover mr = new MarsRover(10, 10, new ArrayList<String>());
		String commandString = "f";
		String res = mr.executeCommand(commandString);
		assertEquals(res, "(0,1,N)");
		commandString = "l";
		res = mr.executeCommand(commandString);
		assertEquals(res, "(0,1,W)");
	}
	
	@Test
	// User story 5 and 7
	public void testGoBackward() throws MarsRoverException {
		MarsRover mr = new MarsRover(10, 10, new ArrayList<String>());
		String commandString = "f";
		String res = mr.executeCommand(commandString);
		assertEquals(res, "(0,1,N)");
		commandString = "b";
		res = mr.executeCommand(commandString);
		assertEquals(res, "(0,0,N)");
		
		String cs = "b";
		String r = mr.executeCommand(cs);
		assertEquals(r, "(0,9,N)");
	}
	
	@Test
	// User story 6
	public void testCompositeCommand() throws MarsRoverException {
		MarsRover mr = new MarsRover(10, 10, new ArrayList<String>());
		String commandString = "ffrff";
		String res = mr.executeCommand(commandString);
		assertEquals(res, "(2,2,E)");
	}
	
	@Test
	// User story 8
	public void testEncounteredObstacle() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(2,2)");
		MarsRover mr = new MarsRover(10, 10, obstacles);
		String commandString = "ffrff";
		String res = mr.executeCommand(commandString);
		assertEquals(res, "(1,2,E)(2,2)");
	}
	
	@Test
	// User story 9
	public void testEncounteredMultipleObstacles() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(2,2)");
		obstacles.add("(2,1)");
		MarsRover mr = new MarsRover(10, 10, obstacles);
		String commandString = "ffrfffrflf";
		String res = mr.executeCommand(commandString);
		assertEquals(res, "(1,1,E)(2,2)(2,1)");
	}
	
	@Test
	// User story 10
	public void testEncounteredCornerObstacle() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(0,9)");
		MarsRover mr = new MarsRover(10, 10, obstacles);
		String commandString = "b";
		String res = mr.executeCommand(commandString);
		assertEquals(res, "(0,0,N)(0,9)");
	}
	
	@Test
	// User story 11
	public void testPlanetTour() throws MarsRoverException {
		List<String> obstacles = new ArrayList<String>();
		obstacles.add("(0,5)");
		obstacles.add("(2,2)");
		obstacles.add("(5,0)");
		MarsRover mr = new MarsRover(6, 6, obstacles);
		String commandString = "ffrfffrbbblllfrfrbbl";
		String res = mr.executeCommand(commandString);
		assertEquals(res, "(0,0,N)(2,2)(0,5)(5,0)");
	}

}
