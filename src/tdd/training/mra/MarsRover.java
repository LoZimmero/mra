package tdd.training.mra;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MarsRover {

	private String[][] planet;
	private List<String> obstacles;
	private String currentRoverFacingDirection;
	private int planetX;
	private int planetY;
	private int currRoverX = 0;
	private int currRoverY = 0;

	public static final String ENEMY_SYMBOL = "X";
	public static final String ROVER_SYMBOL = "R";
	public static final String EMPTY_SYMBOL = " ";

	public static final String ROTATION_RIGHT = "r";
	public static final String ROTATION_LEFT = "l";

	public static final String MOVE_FORWARD = "f";
	public static final String MOVE_BACKWARD = "b";

	public static final String DIRECTION_NORTH = "N";
	public static final String DIRECTION_EAST = "E";
	public static final String DIRECTION_WEST = "W";
	public static final String DIRECTION_SOUTH = "S";

	private final String[] validRotationDirections = new String[] { ROTATION_LEFT, ROTATION_RIGHT };
	private final String[] validMovementDirections = new String[] { MOVE_FORWARD, MOVE_BACKWARD };

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		if (planetX < 1)
			throw new MarsRoverException("X axis cannot be less than 1");
		if (planetY < 1)
			throw new MarsRoverException("Y axis cannot be less than 1");

		this.planetX = planetX;
		this.planetY = planetY;

		planet = new String[planetX][planetY];
		for (int i = 0; i < planetX; i++) {
			for (int j = 0; j < planetY; j++) {
				planet[i][j] = MarsRover.EMPTY_SYMBOL;
			}
		}
		planet[0][0] = MarsRover.ROVER_SYMBOL; // Place rover in orign
		currentRoverFacingDirection = MarsRover.DIRECTION_NORTH; // Rover faces North initially

		if (planetObstacles != null) {
			for (String s : planetObstacles) {
				addObstacle(s);
			}
		}

	}

	private void rotate(String direction) throws MarsRoverException {
		if (direction == null)
			throw new MarsRoverException("Null rotation direction");
		if (direction.equals(ROTATION_RIGHT)) {
			// Rotate right
			if (currentRoverFacingDirection.equals(DIRECTION_NORTH))
				currentRoverFacingDirection = DIRECTION_EAST;
			else if (currentRoverFacingDirection.equals(DIRECTION_EAST))
				currentRoverFacingDirection = DIRECTION_SOUTH;
			else if (currentRoverFacingDirection.equals(DIRECTION_SOUTH))
				currentRoverFacingDirection = DIRECTION_WEST;
			else if (currentRoverFacingDirection.equals(DIRECTION_WEST))
				currentRoverFacingDirection = DIRECTION_NORTH;
		} else if (direction.equals(ROTATION_LEFT)) {
			// Rotate left
			if (currentRoverFacingDirection.equals(DIRECTION_NORTH))
				currentRoverFacingDirection = DIRECTION_WEST;
			else if (currentRoverFacingDirection.equals(DIRECTION_EAST))
				currentRoverFacingDirection = DIRECTION_NORTH;
			else if (currentRoverFacingDirection.equals(DIRECTION_SOUTH))
				currentRoverFacingDirection = DIRECTION_EAST;
			else if (currentRoverFacingDirection.equals(DIRECTION_WEST))
				currentRoverFacingDirection = DIRECTION_SOUTH;
		} else
			throw new MarsRoverException("Invalid rotation direction");
	}

	private String move(String direction) throws MarsRoverException {
		String res = null;
		int newX = 0;
		int newY = 0;
		if (direction.equals(MOVE_FORWARD)) {
			switch (currentRoverFacingDirection) {
			case DIRECTION_NORTH:
				newY = (currRoverY + 1) % planetY;
				if (planet[currRoverX][newY] == ENEMY_SYMBOL) {
					res = "("+currRoverX+","+newY+")";
				} else {
					this.planet[currRoverX][currRoverY] = EMPTY_SYMBOL;
					this.planet[currRoverX][newY] = ROVER_SYMBOL;
					currRoverY = newY;
				}
				break;
			case DIRECTION_EAST:
				newX = (currRoverX + 1) % planetX;
				if (planet[newX][currRoverY] == ENEMY_SYMBOL) {
					res = "("+newX+","+currRoverY+")";
				} else {
					this.planet[currRoverX][currRoverY] = EMPTY_SYMBOL;
					this.planet[newX][currRoverY] = ROVER_SYMBOL;
					currRoverX = newX;
				}
				break;
			case DIRECTION_WEST:
				newX = currRoverX - 1;
				if (newX < 0)
					newX = (planetX - 1);
				if (planet[newX][currRoverY] == ENEMY_SYMBOL) {
					res = "("+newX+","+currRoverY+")";
				} else {
					this.planet[currRoverX][currRoverY] = EMPTY_SYMBOL;
					this.planet[newX][currRoverY] = ROVER_SYMBOL;
					currRoverX = newX;
				}
				break;
			case DIRECTION_SOUTH:
				newY = currRoverY - 1;
				if (newY < 0)
					newY = (planetY - 1);
				if (planet[currRoverX][newY] == ENEMY_SYMBOL) {
					res = "("+currRoverX+","+newY+")";
				} else {
					this.planet[currRoverX][currRoverY] = EMPTY_SYMBOL;
					this.planet[currRoverX][newY] = ROVER_SYMBOL;
					currRoverY = newY;
				}
				break;
			default:
				throw new MarsRoverException("Illegal rover state");
			}
		} else if (direction.equals(MOVE_BACKWARD)) {
			switch (currentRoverFacingDirection) {
			case DIRECTION_SOUTH:
				newY = (currRoverY + 1) % planetY;
				if (planet[currRoverX][newY] == ENEMY_SYMBOL) {
					res = "("+currRoverX+","+newY+")";
				} else {
					this.planet[currRoverX][currRoverY] = EMPTY_SYMBOL;
					this.planet[currRoverX][newY] = ROVER_SYMBOL;
					currRoverY = newY;
				}
				break;
			case DIRECTION_WEST:
				newX = (currRoverX + 1) % planetX;
				if (planet[newX][currRoverY] == ENEMY_SYMBOL) {
					res = "("+newX+","+currRoverY+")";
				} else {
					this.planet[currRoverX][currRoverY] = EMPTY_SYMBOL;
					this.planet[newX][currRoverY] = ROVER_SYMBOL;
					currRoverX = newX;
				}
				break;
			case DIRECTION_EAST:
				newX = currRoverX - 1;
				if (newX < 0)
					newX = (planetX - 1);
				if (planet[newX][currRoverY] == ENEMY_SYMBOL) {
					res = "("+newX+","+currRoverY+")";
				} else {
					this.planet[currRoverX][currRoverY] = EMPTY_SYMBOL;
					this.planet[newX][currRoverY] = ROVER_SYMBOL;
					currRoverX = newX;
				}
				break;
			case DIRECTION_NORTH:
				newY = currRoverY - 1;
				if (newY < 0)
					newY = (planetY - 1);
				if (planet[currRoverX][newY] == ENEMY_SYMBOL) {
					res = "("+currRoverX+","+newY+")";
				} else {
					this.planet[currRoverX][currRoverY] = EMPTY_SYMBOL;
					this.planet[currRoverX][newY] = ROVER_SYMBOL;
					currRoverY = newY;
				}
				break;
			default:
				throw new MarsRoverException("Illegal rover state");
			}
		}
		return res;
	}

	private void addObstacle(String obstacleString) throws MarsRoverException {
		Pattern p = Pattern.compile("^\\(([0-9]+),([0-9]+)\\)$");
		String s = obstacleString.strip(); // Remove empty spaces
		Matcher m = p.matcher(s);
		if (m.find()) {
			try {
				int x = Integer.parseInt(m.group(1));
				int y = Integer.parseInt(m.group(2));
				if (planet[x][y] != MarsRover.EMPTY_SYMBOL)
					throw new MarsRoverException("Cannot place obstacle on other thing");
				planet[x][y] = MarsRover.ENEMY_SYMBOL;
			} catch (Exception e) {
				throw new MarsRoverException("Invalid Obstacle position");
			}
		} else {
			throw new MarsRoverException("Invalid obstacle string");
		}

	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		try {
			boolean result = (planet[x][y] == MarsRover.ENEMY_SYMBOL);
			return result;
		} catch (Exception e) {
			// IndexOutOfBoundException --> In this case there isn't an obstacle
			return false;
		}
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		String res = null;
		String obstacles = "";
		if (commandString == null)
			throw new MarsRoverException("Null command recieved");
		if (commandString.equalsIgnoreCase(""))
			res = "(0,0,N)";

		for (String c : commandString.split("")) {
			if (Arrays.asList(validRotationDirections).contains(c)) {
				rotate(c);
			}

			if (Arrays.asList(validMovementDirections).contains(c)) {
				String obstacleFound = move(c);
				if (obstacleFound != null && !obstacles.contains(obstacleFound)) obstacles += obstacleFound;
			}
		}

		res = "(" + currRoverX + "," + currRoverY + "," + currentRoverFacingDirection + ")"+obstacles;
		return res;
	}

}
